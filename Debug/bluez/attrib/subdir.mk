################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../bluez/attrib/att.c \
../bluez/attrib/gatt.c \
../bluez/attrib/gattrib.c \
../bluez/attrib/interactive.c \
../bluez/attrib/utils.c 

OBJS += \
./bluez/attrib/att.o \
./bluez/attrib/gatt.o \
./bluez/attrib/gattrib.o \
./bluez/attrib/interactive.o \
./bluez/attrib/utils.o 

C_DEPS += \
./bluez/attrib/att.d \
./bluez/attrib/gatt.d \
./bluez/attrib/gattrib.d \
./bluez/attrib/interactive.d \
./bluez/attrib/utils.d 


# Each subdirectory must supply rules for building sources it contributes
bluez/attrib/%.o: ../bluez/attrib/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -DVERSION='"DUMB"' -DSTORAGEDIR='"/usr/local/var/lib/bluetooth"' -D_GNU_SOURCE -DCONFIGDIR='"/usr/local/etc/bluetooth"' -DPLUGINDIR='"/home/jagomo/eclipse-workspace/bletest/src/bluez/plugins"' -DBLUETOOTH_PLUGIN_BUILTIN -I/usr/include/glib-2.0 -I../usr/include/gtk-2.0 -I/usr/include/glib-2.0/glib -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/dbus-1.0/dbus -I/usr/include/dbus-1.0 -I/usr/lib/x86_64-linux-gnu/dbus-1.0/include -I"/home/jagomo/eclipse-workspace/bletest" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


