################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../bluez/adapter.c \
../bluez/advertising.c \
../bluez/agent.c \
../bluez/attrib-server.c \
../bluez/backtrace.c \
../bluez/dbus-common.c \
../bluez/device.c \
../bluez/eir.c \
../bluez/error.c \
../bluez/gatt-client.c \
../bluez/gatt-database.c \
../bluez/log.c \
../bluez/main.c \
../bluez/oui.c \
../bluez/plugin.c \
../bluez/profile.c \
../bluez/rfkill.c \
../bluez/sdp-client.c \
../bluez/sdp-xml.c \
../bluez/sdpd-database.c \
../bluez/sdpd-request.c \
../bluez/sdpd-server.c \
../bluez/sdpd-service.c \
../bluez/service.c \
../bluez/storage.c \
../bluez/systemd.c \
../bluez/textfile.c \
../bluez/uuid-helper.c 

OBJS += \
./bluez/adapter.o \
./bluez/advertising.o \
./bluez/agent.o \
./bluez/attrib-server.o \
./bluez/backtrace.o \
./bluez/dbus-common.o \
./bluez/device.o \
./bluez/eir.o \
./bluez/error.o \
./bluez/gatt-client.o \
./bluez/gatt-database.o \
./bluez/log.o \
./bluez/main.o \
./bluez/oui.o \
./bluez/plugin.o \
./bluez/profile.o \
./bluez/rfkill.o \
./bluez/sdp-client.o \
./bluez/sdp-xml.o \
./bluez/sdpd-database.o \
./bluez/sdpd-request.o \
./bluez/sdpd-server.o \
./bluez/sdpd-service.o \
./bluez/service.o \
./bluez/storage.o \
./bluez/systemd.o \
./bluez/textfile.o \
./bluez/uuid-helper.o 

C_DEPS += \
./bluez/adapter.d \
./bluez/advertising.d \
./bluez/agent.d \
./bluez/attrib-server.d \
./bluez/backtrace.d \
./bluez/dbus-common.d \
./bluez/device.d \
./bluez/eir.d \
./bluez/error.d \
./bluez/gatt-client.d \
./bluez/gatt-database.d \
./bluez/log.d \
./bluez/main.d \
./bluez/oui.d \
./bluez/plugin.d \
./bluez/profile.d \
./bluez/rfkill.d \
./bluez/sdp-client.d \
./bluez/sdp-xml.d \
./bluez/sdpd-database.d \
./bluez/sdpd-request.d \
./bluez/sdpd-server.d \
./bluez/sdpd-service.d \
./bluez/service.d \
./bluez/storage.d \
./bluez/systemd.d \
./bluez/textfile.d \
./bluez/uuid-helper.d 


# Each subdirectory must supply rules for building sources it contributes
bluez/%.o: ../bluez/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -DVERSION='"DUMB"' -DSTORAGEDIR='"/usr/local/var/lib/bluetooth"' -D_GNU_SOURCE -DCONFIGDIR='"/usr/local/etc/bluetooth"' -DPLUGINDIR='"/home/jagomo/eclipse-workspace/bletest/src/bluez/plugins"' -DBLUETOOTH_PLUGIN_BUILTIN -I/usr/include/glib-2.0 -I../usr/include/gtk-2.0 -I/usr/include/glib-2.0/glib -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/dbus-1.0/dbus -I/usr/include/dbus-1.0 -I/usr/lib/x86_64-linux-gnu/dbus-1.0/include -I"/home/jagomo/eclipse-workspace/bletest" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


