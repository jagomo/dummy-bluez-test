################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../bluez/src/backtrace.c \
../bluez/src/dbus-common.c \
../bluez/src/eir.c \
../bluez/src/error.c \
../bluez/src/log.c \
../bluez/src/oui.c \
../bluez/src/sdp-client.c \
../bluez/src/sdp-xml.c \
../bluez/src/sdpd-database.c \
../bluez/src/sdpd-request.c \
../bluez/src/sdpd-server.c \
../bluez/src/sdpd-service.c \
../bluez/src/storage.c \
../bluez/src/systemd.c \
../bluez/src/textfile.c \
../bluez/src/uuid-helper.c 

OBJS += \
./bluez/src/backtrace.o \
./bluez/src/dbus-common.o \
./bluez/src/eir.o \
./bluez/src/error.o \
./bluez/src/log.o \
./bluez/src/oui.o \
./bluez/src/sdp-client.o \
./bluez/src/sdp-xml.o \
./bluez/src/sdpd-database.o \
./bluez/src/sdpd-request.o \
./bluez/src/sdpd-server.o \
./bluez/src/sdpd-service.o \
./bluez/src/storage.o \
./bluez/src/systemd.o \
./bluez/src/textfile.o \
./bluez/src/uuid-helper.o 

C_DEPS += \
./bluez/src/backtrace.d \
./bluez/src/dbus-common.d \
./bluez/src/eir.d \
./bluez/src/error.d \
./bluez/src/log.d \
./bluez/src/oui.d \
./bluez/src/sdp-client.d \
./bluez/src/sdp-xml.d \
./bluez/src/sdpd-database.d \
./bluez/src/sdpd-request.d \
./bluez/src/sdpd-server.d \
./bluez/src/sdpd-service.d \
./bluez/src/storage.d \
./bluez/src/systemd.d \
./bluez/src/textfile.d \
./bluez/src/uuid-helper.d 


# Each subdirectory must supply rules for building sources it contributes
bluez/src/%.o: ../bluez/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -DVERSION='"DUMB"' -DSTORAGEDIR='"/usr/local/var/lib/bluetooth"' -D_GNU_SOURCE -DCONFIGDIR='"/usr/local/etc/bluetooth"' -DPLUGINDIR='"/home/jagomo/eclipse-workspace/bletest/src/bluez/plugins"' -DBLUETOOTH_PLUGIN_BUILTIN -I/usr/include/glib-2.0 -I../usr/include/gtk-2.0 -I/usr/include/glib-2.0/glib -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/dbus-1.0/dbus -I/usr/include/dbus-1.0 -I/usr/lib/x86_64-linux-gnu/dbus-1.0/include -I"/home/jagomo/eclipse-workspace/bletest" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


