################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../bluez/src/shared/ad.c \
../bluez/src/shared/att.c \
../bluez/src/shared/btsnoop.c \
../bluez/src/shared/crypto.c \
../bluez/src/shared/ecc.c \
../bluez/src/shared/gap.c \
../bluez/src/shared/gatt-client.c \
../bluez/src/shared/gatt-db.c \
../bluez/src/shared/gatt-helpers.c \
../bluez/src/shared/gatt-server.c \
../bluez/src/shared/hci-crypto.c \
../bluez/src/shared/hci.c \
../bluez/src/shared/hfp.c \
../bluez/src/shared/io-glib.c \
../bluez/src/shared/mainloop.c \
../bluez/src/shared/mgmt.c \
../bluez/src/shared/pcap.c \
../bluez/src/shared/queue.c \
../bluez/src/shared/ringbuf.c \
../bluez/src/shared/shell.c \
../bluez/src/shared/tester.c \
../bluez/src/shared/timeout-glib.c \
../bluez/src/shared/uhid.c \
../bluez/src/shared/util.c 

OBJS += \
./bluez/src/shared/ad.o \
./bluez/src/shared/att.o \
./bluez/src/shared/btsnoop.o \
./bluez/src/shared/crypto.o \
./bluez/src/shared/ecc.o \
./bluez/src/shared/gap.o \
./bluez/src/shared/gatt-client.o \
./bluez/src/shared/gatt-db.o \
./bluez/src/shared/gatt-helpers.o \
./bluez/src/shared/gatt-server.o \
./bluez/src/shared/hci-crypto.o \
./bluez/src/shared/hci.o \
./bluez/src/shared/hfp.o \
./bluez/src/shared/io-glib.o \
./bluez/src/shared/mainloop.o \
./bluez/src/shared/mgmt.o \
./bluez/src/shared/pcap.o \
./bluez/src/shared/queue.o \
./bluez/src/shared/ringbuf.o \
./bluez/src/shared/shell.o \
./bluez/src/shared/tester.o \
./bluez/src/shared/timeout-glib.o \
./bluez/src/shared/uhid.o \
./bluez/src/shared/util.o 

C_DEPS += \
./bluez/src/shared/ad.d \
./bluez/src/shared/att.d \
./bluez/src/shared/btsnoop.d \
./bluez/src/shared/crypto.d \
./bluez/src/shared/ecc.d \
./bluez/src/shared/gap.d \
./bluez/src/shared/gatt-client.d \
./bluez/src/shared/gatt-db.d \
./bluez/src/shared/gatt-helpers.d \
./bluez/src/shared/gatt-server.d \
./bluez/src/shared/hci-crypto.d \
./bluez/src/shared/hci.d \
./bluez/src/shared/hfp.d \
./bluez/src/shared/io-glib.d \
./bluez/src/shared/mainloop.d \
./bluez/src/shared/mgmt.d \
./bluez/src/shared/pcap.d \
./bluez/src/shared/queue.d \
./bluez/src/shared/ringbuf.d \
./bluez/src/shared/shell.d \
./bluez/src/shared/tester.d \
./bluez/src/shared/timeout-glib.d \
./bluez/src/shared/uhid.d \
./bluez/src/shared/util.d 


# Each subdirectory must supply rules for building sources it contributes
bluez/src/shared/%.o: ../bluez/src/shared/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -DVERSION='"DUMB"' -DSTORAGEDIR='"/usr/local/var/lib/bluetooth"' -D_GNU_SOURCE -DCONFIGDIR='"/usr/local/etc/bluetooth"' -DPLUGINDIR='"/home/jagomo/eclipse-workspace/bletest/src/bluez/plugins"' -DBLUETOOTH_PLUGIN_BUILTIN -I/usr/include/glib-2.0 -I../usr/include/gtk-2.0 -I/usr/include/glib-2.0/glib -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/dbus-1.0/dbus -I/usr/include/dbus-1.0 -I/usr/lib/x86_64-linux-gnu/dbus-1.0/include -I"/home/jagomo/eclipse-workspace/bletest" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


