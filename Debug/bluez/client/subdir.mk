################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../bluez/client/advertising.c \
../bluez/client/agent.c \
../bluez/client/display.c \
../bluez/client/gatt.c 

OBJS += \
./bluez/client/advertising.o \
./bluez/client/agent.o \
./bluez/client/display.o \
./bluez/client/gatt.o 

C_DEPS += \
./bluez/client/advertising.d \
./bluez/client/agent.d \
./bluez/client/display.d \
./bluez/client/gatt.d 


# Each subdirectory must supply rules for building sources it contributes
bluez/client/%.o: ../bluez/client/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -DVERSION='"DUMB"' -DSTORAGEDIR='"/usr/local/var/lib/bluetooth"' -D_GNU_SOURCE -DCONFIGDIR='"/usr/local/etc/bluetooth"' -DPLUGINDIR='"/home/jagomo/eclipse-workspace/bletest/src/bluez/plugins"' -DBLUETOOTH_PLUGIN_BUILTIN -I/usr/include/glib-2.0 -I../usr/include/gtk-2.0 -I/usr/include/glib-2.0/glib -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/dbus-1.0/dbus -I/usr/include/dbus-1.0 -I/usr/lib/x86_64-linux-gnu/dbus-1.0/include -I"/home/jagomo/eclipse-workspace/bletest" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


