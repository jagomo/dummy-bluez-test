################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../bluez/monitor/a2dp.c \
../bluez/monitor/analyze.c \
../bluez/monitor/avctp.c \
../bluez/monitor/avdtp.c \
../bluez/monitor/bnep.c \
../bluez/monitor/broadcom.c \
../bluez/monitor/control.c \
../bluez/monitor/crc.c \
../bluez/monitor/display.c \
../bluez/monitor/ellisys.c \
../bluez/monitor/hcidump.c \
../bluez/monitor/hwdb.c \
../bluez/monitor/intel.c \
../bluez/monitor/keys.c \
../bluez/monitor/l2cap.c \
../bluez/monitor/ll.c \
../bluez/monitor/lmp.c \
../bluez/monitor/packet.c \
../bluez/monitor/rfcomm.c \
../bluez/monitor/sdp.c \
../bluez/monitor/vendor.c 

OBJS += \
./bluez/monitor/a2dp.o \
./bluez/monitor/analyze.o \
./bluez/monitor/avctp.o \
./bluez/monitor/avdtp.o \
./bluez/monitor/bnep.o \
./bluez/monitor/broadcom.o \
./bluez/monitor/control.o \
./bluez/monitor/crc.o \
./bluez/monitor/display.o \
./bluez/monitor/ellisys.o \
./bluez/monitor/hcidump.o \
./bluez/monitor/hwdb.o \
./bluez/monitor/intel.o \
./bluez/monitor/keys.o \
./bluez/monitor/l2cap.o \
./bluez/monitor/ll.o \
./bluez/monitor/lmp.o \
./bluez/monitor/packet.o \
./bluez/monitor/rfcomm.o \
./bluez/monitor/sdp.o \
./bluez/monitor/vendor.o 

C_DEPS += \
./bluez/monitor/a2dp.d \
./bluez/monitor/analyze.d \
./bluez/monitor/avctp.d \
./bluez/monitor/avdtp.d \
./bluez/monitor/bnep.d \
./bluez/monitor/broadcom.d \
./bluez/monitor/control.d \
./bluez/monitor/crc.d \
./bluez/monitor/display.d \
./bluez/monitor/ellisys.d \
./bluez/monitor/hcidump.d \
./bluez/monitor/hwdb.d \
./bluez/monitor/intel.d \
./bluez/monitor/keys.d \
./bluez/monitor/l2cap.d \
./bluez/monitor/ll.d \
./bluez/monitor/lmp.d \
./bluez/monitor/packet.d \
./bluez/monitor/rfcomm.d \
./bluez/monitor/sdp.d \
./bluez/monitor/vendor.d 


# Each subdirectory must supply rules for building sources it contributes
bluez/monitor/%.o: ../bluez/monitor/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -DVERSION='"DUMB"' -DSTORAGEDIR='"/usr/local/var/lib/bluetooth"' -D_GNU_SOURCE -DCONFIGDIR='"/usr/local/etc/bluetooth"' -DPLUGINDIR='"/home/jagomo/eclipse-workspace/bletest/src/bluez/plugins"' -DBLUETOOTH_PLUGIN_BUILTIN -I/usr/include/glib-2.0 -I../usr/include/gtk-2.0 -I/usr/include/glib-2.0/glib -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/dbus-1.0/dbus -I/usr/include/dbus-1.0 -I/usr/lib/x86_64-linux-gnu/dbus-1.0/include -I"/home/jagomo/eclipse-workspace/bletest" -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


