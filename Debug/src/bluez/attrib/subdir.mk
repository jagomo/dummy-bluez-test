################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/bluez/attrib/att.c \
../src/bluez/attrib/gatt-service.c \
../src/bluez/attrib/gatt.c \
../src/bluez/attrib/gattrib.c \
../src/bluez/attrib/gatttool.c \
../src/bluez/attrib/interactive.c \
../src/bluez/attrib/utils.c 

OBJS += \
./src/bluez/attrib/att.o \
./src/bluez/attrib/gatt-service.o \
./src/bluez/attrib/gatt.o \
./src/bluez/attrib/gattrib.o \
./src/bluez/attrib/gatttool.o \
./src/bluez/attrib/interactive.o \
./src/bluez/attrib/utils.o 

C_DEPS += \
./src/bluez/attrib/att.d \
./src/bluez/attrib/gatt-service.d \
./src/bluez/attrib/gatt.d \
./src/bluez/attrib/gattrib.d \
./src/bluez/attrib/gatttool.d \
./src/bluez/attrib/interactive.d \
./src/bluez/attrib/utils.d 


# Each subdirectory must supply rules for building sources it contributes
src/bluez/attrib/%.o: ../src/bluez/attrib/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -DVERSION='"DUMB"' -DSTORAGEDIR='"/usr/local/var/lib/bluetooth"' -D_GNU_SOURCE -DCONFIGDIR='"/usr/local/etc/bluetooth"' -DPLUGINDIR='"/home/jagomo/eclipse-workspace/bletest/src/bluez/plugins"' -DBLUETOOTH_PLUGIN_BUILTIN -I/usr/include/glib-2.0 -I../usr/include/gtk-2.0 -I/usr/include/glib-2.0/glib -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/dbus-1.0/dbus -I/usr/include/dbus-1.0 -I/usr/lib/x86_64-linux-gnu/dbus-1.0/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


