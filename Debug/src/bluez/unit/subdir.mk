################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/bluez/unit/test-avctp.c \
../src/bluez/unit/test-avdtp.c \
../src/bluez/unit/test-avrcp.c \
../src/bluez/unit/test-crc.c \
../src/bluez/unit/test-crypto.c \
../src/bluez/unit/test-ecc.c \
../src/bluez/unit/test-eir.c \
../src/bluez/unit/test-gatt.c \
../src/bluez/unit/test-gattrib.c \
../src/bluez/unit/test-gdbus-client.c \
../src/bluez/unit/test-gobex-apparam.c \
../src/bluez/unit/test-gobex-header.c \
../src/bluez/unit/test-gobex-packet.c \
../src/bluez/unit/test-gobex-transfer.c \
../src/bluez/unit/test-gobex.c \
../src/bluez/unit/test-hfp.c \
../src/bluez/unit/test-hog.c \
../src/bluez/unit/test-lib.c \
../src/bluez/unit/test-mgmt.c \
../src/bluez/unit/test-midi.c \
../src/bluez/unit/test-queue.c \
../src/bluez/unit/test-ringbuf.c \
../src/bluez/unit/test-sdp.c \
../src/bluez/unit/test-textfile.c \
../src/bluez/unit/test-uhid.c \
../src/bluez/unit/test-uuid.c \
../src/bluez/unit/util.c 

OBJS += \
./src/bluez/unit/test-avctp.o \
./src/bluez/unit/test-avdtp.o \
./src/bluez/unit/test-avrcp.o \
./src/bluez/unit/test-crc.o \
./src/bluez/unit/test-crypto.o \
./src/bluez/unit/test-ecc.o \
./src/bluez/unit/test-eir.o \
./src/bluez/unit/test-gatt.o \
./src/bluez/unit/test-gattrib.o \
./src/bluez/unit/test-gdbus-client.o \
./src/bluez/unit/test-gobex-apparam.o \
./src/bluez/unit/test-gobex-header.o \
./src/bluez/unit/test-gobex-packet.o \
./src/bluez/unit/test-gobex-transfer.o \
./src/bluez/unit/test-gobex.o \
./src/bluez/unit/test-hfp.o \
./src/bluez/unit/test-hog.o \
./src/bluez/unit/test-lib.o \
./src/bluez/unit/test-mgmt.o \
./src/bluez/unit/test-midi.o \
./src/bluez/unit/test-queue.o \
./src/bluez/unit/test-ringbuf.o \
./src/bluez/unit/test-sdp.o \
./src/bluez/unit/test-textfile.o \
./src/bluez/unit/test-uhid.o \
./src/bluez/unit/test-uuid.o \
./src/bluez/unit/util.o 

C_DEPS += \
./src/bluez/unit/test-avctp.d \
./src/bluez/unit/test-avdtp.d \
./src/bluez/unit/test-avrcp.d \
./src/bluez/unit/test-crc.d \
./src/bluez/unit/test-crypto.d \
./src/bluez/unit/test-ecc.d \
./src/bluez/unit/test-eir.d \
./src/bluez/unit/test-gatt.d \
./src/bluez/unit/test-gattrib.d \
./src/bluez/unit/test-gdbus-client.d \
./src/bluez/unit/test-gobex-apparam.d \
./src/bluez/unit/test-gobex-header.d \
./src/bluez/unit/test-gobex-packet.d \
./src/bluez/unit/test-gobex-transfer.d \
./src/bluez/unit/test-gobex.d \
./src/bluez/unit/test-hfp.d \
./src/bluez/unit/test-hog.d \
./src/bluez/unit/test-lib.d \
./src/bluez/unit/test-mgmt.d \
./src/bluez/unit/test-midi.d \
./src/bluez/unit/test-queue.d \
./src/bluez/unit/test-ringbuf.d \
./src/bluez/unit/test-sdp.d \
./src/bluez/unit/test-textfile.d \
./src/bluez/unit/test-uhid.d \
./src/bluez/unit/test-uuid.d \
./src/bluez/unit/util.d 


# Each subdirectory must supply rules for building sources it contributes
src/bluez/unit/%.o: ../src/bluez/unit/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -DVERSION='"DUMB"' -DSTORAGEDIR='"/usr/local/var/lib/bluetooth"' -D_GNU_SOURCE -DCONFIGDIR='"/usr/local/etc/bluetooth"' -DPLUGINDIR='"/home/jagomo/eclipse-workspace/bletest/src/bluez/plugins"' -DBLUETOOTH_PLUGIN_BUILTIN -I/usr/include/glib-2.0 -I../usr/include/gtk-2.0 -I/usr/include/glib-2.0/glib -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/dbus-1.0/dbus -I/usr/include/dbus-1.0 -I/usr/lib/x86_64-linux-gnu/dbus-1.0/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


