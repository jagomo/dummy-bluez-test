################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/bluez/gobex/gobex-apparam.c \
../src/bluez/gobex/gobex-defs.c \
../src/bluez/gobex/gobex-header.c \
../src/bluez/gobex/gobex-packet.c \
../src/bluez/gobex/gobex-transfer.c \
../src/bluez/gobex/gobex.c 

OBJS += \
./src/bluez/gobex/gobex-apparam.o \
./src/bluez/gobex/gobex-defs.o \
./src/bluez/gobex/gobex-header.o \
./src/bluez/gobex/gobex-packet.o \
./src/bluez/gobex/gobex-transfer.o \
./src/bluez/gobex/gobex.o 

C_DEPS += \
./src/bluez/gobex/gobex-apparam.d \
./src/bluez/gobex/gobex-defs.d \
./src/bluez/gobex/gobex-header.d \
./src/bluez/gobex/gobex-packet.d \
./src/bluez/gobex/gobex-transfer.d \
./src/bluez/gobex/gobex.d 


# Each subdirectory must supply rules for building sources it contributes
src/bluez/gobex/%.o: ../src/bluez/gobex/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -DVERSION='"DUMB"' -DSTORAGEDIR='"/usr/local/var/lib/bluetooth"' -D_GNU_SOURCE -DCONFIGDIR='"/usr/local/etc/bluetooth"' -DPLUGINDIR='"/home/jagomo/eclipse-workspace/bletest/src/bluez/plugins"' -DBLUETOOTH_PLUGIN_BUILTIN -I/usr/include/glib-2.0 -I../usr/include/gtk-2.0 -I/usr/include/glib-2.0/glib -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/dbus-1.0/dbus -I/usr/include/dbus-1.0 -I/usr/lib/x86_64-linux-gnu/dbus-1.0/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


