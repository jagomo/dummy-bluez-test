################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/bluez/profiles/input/device.c \
../src/bluez/profiles/input/hog-lib.c \
../src/bluez/profiles/input/hog.c \
../src/bluez/profiles/input/manager.c \
../src/bluez/profiles/input/server.c \
../src/bluez/profiles/input/suspend-dummy.c \
../src/bluez/profiles/input/suspend-none.c 

OBJS += \
./src/bluez/profiles/input/device.o \
./src/bluez/profiles/input/hog-lib.o \
./src/bluez/profiles/input/hog.o \
./src/bluez/profiles/input/manager.o \
./src/bluez/profiles/input/server.o \
./src/bluez/profiles/input/suspend-dummy.o \
./src/bluez/profiles/input/suspend-none.o 

C_DEPS += \
./src/bluez/profiles/input/device.d \
./src/bluez/profiles/input/hog-lib.d \
./src/bluez/profiles/input/hog.d \
./src/bluez/profiles/input/manager.d \
./src/bluez/profiles/input/server.d \
./src/bluez/profiles/input/suspend-dummy.d \
./src/bluez/profiles/input/suspend-none.d 


# Each subdirectory must supply rules for building sources it contributes
src/bluez/profiles/input/%.o: ../src/bluez/profiles/input/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -DVERSION='"DUMB"' -DSTORAGEDIR='"/usr/local/var/lib/bluetooth"' -D_GNU_SOURCE -DCONFIGDIR='"/usr/local/etc/bluetooth"' -DPLUGINDIR='"/home/jagomo/eclipse-workspace/bletest/src/bluez/plugins"' -DBLUETOOTH_PLUGIN_BUILTIN -I/usr/include/glib-2.0 -I../usr/include/gtk-2.0 -I/usr/include/glib-2.0/glib -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/dbus-1.0/dbus -I/usr/include/dbus-1.0 -I/usr/lib/x86_64-linux-gnu/dbus-1.0/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


