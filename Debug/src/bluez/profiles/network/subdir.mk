################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/bluez/profiles/network/bnep.c \
../src/bluez/profiles/network/connection.c \
../src/bluez/profiles/network/manager.c \
../src/bluez/profiles/network/server.c 

OBJS += \
./src/bluez/profiles/network/bnep.o \
./src/bluez/profiles/network/connection.o \
./src/bluez/profiles/network/manager.o \
./src/bluez/profiles/network/server.o 

C_DEPS += \
./src/bluez/profiles/network/bnep.d \
./src/bluez/profiles/network/connection.d \
./src/bluez/profiles/network/manager.d \
./src/bluez/profiles/network/server.d 


# Each subdirectory must supply rules for building sources it contributes
src/bluez/profiles/network/%.o: ../src/bluez/profiles/network/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -DVERSION='"DUMB"' -DSTORAGEDIR='"/usr/local/var/lib/bluetooth"' -D_GNU_SOURCE -DCONFIGDIR='"/usr/local/etc/bluetooth"' -DPLUGINDIR='"/home/jagomo/eclipse-workspace/bletest/src/bluez/plugins"' -DBLUETOOTH_PLUGIN_BUILTIN -I/usr/include/glib-2.0 -I../usr/include/gtk-2.0 -I/usr/include/glib-2.0/glib -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/dbus-1.0/dbus -I/usr/include/dbus-1.0 -I/usr/lib/x86_64-linux-gnu/dbus-1.0/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


