################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/bluez/profiles/sap/main.c \
../src/bluez/profiles/sap/manager.c \
../src/bluez/profiles/sap/sap-dummy.c \
../src/bluez/profiles/sap/server.c 

OBJS += \
./src/bluez/profiles/sap/main.o \
./src/bluez/profiles/sap/manager.o \
./src/bluez/profiles/sap/sap-dummy.o \
./src/bluez/profiles/sap/server.o 

C_DEPS += \
./src/bluez/profiles/sap/main.d \
./src/bluez/profiles/sap/manager.d \
./src/bluez/profiles/sap/sap-dummy.d \
./src/bluez/profiles/sap/server.d 


# Each subdirectory must supply rules for building sources it contributes
src/bluez/profiles/sap/%.o: ../src/bluez/profiles/sap/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -DVERSION='"DUMB"' -DSTORAGEDIR='"/usr/local/var/lib/bluetooth"' -D_GNU_SOURCE -DCONFIGDIR='"/usr/local/etc/bluetooth"' -DPLUGINDIR='"/home/jagomo/eclipse-workspace/bletest/src/bluez/plugins"' -I/usr/include/glib-2.0 -I../usr/include/gtk-2.0 -I/usr/include/glib-2.0/glib -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/dbus-1.0/dbus -I/usr/include/dbus-1.0 -I/usr/lib/x86_64-linux-gnu/dbus-1.0/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


