################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/bluez/profiles/cups/hcrp.c \
../src/bluez/profiles/cups/main.c \
../src/bluez/profiles/cups/sdp.c \
../src/bluez/profiles/cups/spp.c 

OBJS += \
./src/bluez/profiles/cups/hcrp.o \
./src/bluez/profiles/cups/main.o \
./src/bluez/profiles/cups/sdp.o \
./src/bluez/profiles/cups/spp.o 

C_DEPS += \
./src/bluez/profiles/cups/hcrp.d \
./src/bluez/profiles/cups/main.d \
./src/bluez/profiles/cups/sdp.d \
./src/bluez/profiles/cups/spp.d 


# Each subdirectory must supply rules for building sources it contributes
src/bluez/profiles/cups/%.o: ../src/bluez/profiles/cups/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -DVERSION='"DUMB"' -DSTORAGEDIR='"/usr/local/var/lib/bluetooth"' -D_GNU_SOURCE -DCONFIGDIR='"/usr/local/etc/bluetooth"' -DPLUGINDIR='"/home/jagomo/eclipse-workspace/bletest/src/bluez/plugins"' -DBLUETOOTH_PLUGIN_BUILTIN -I/usr/include/glib-2.0 -I../usr/include/gtk-2.0 -I/usr/include/glib-2.0/glib -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/dbus-1.0/dbus -I/usr/include/dbus-1.0 -I/usr/lib/x86_64-linux-gnu/dbus-1.0/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


