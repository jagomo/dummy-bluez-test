################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/bluez/profiles/health/hdp.c \
../src/bluez/profiles/health/hdp_main.c \
../src/bluez/profiles/health/hdp_manager.c \
../src/bluez/profiles/health/hdp_util.c \
../src/bluez/profiles/health/mcap.c 

OBJS += \
./src/bluez/profiles/health/hdp.o \
./src/bluez/profiles/health/hdp_main.o \
./src/bluez/profiles/health/hdp_manager.o \
./src/bluez/profiles/health/hdp_util.o \
./src/bluez/profiles/health/mcap.o 

C_DEPS += \
./src/bluez/profiles/health/hdp.d \
./src/bluez/profiles/health/hdp_main.d \
./src/bluez/profiles/health/hdp_manager.d \
./src/bluez/profiles/health/hdp_util.d \
./src/bluez/profiles/health/mcap.d 


# Each subdirectory must supply rules for building sources it contributes
src/bluez/profiles/health/%.o: ../src/bluez/profiles/health/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -DVERSION='"DUMB"' -DSTORAGEDIR='"/usr/local/var/lib/bluetooth"' -D_GNU_SOURCE -DCONFIGDIR='"/usr/local/etc/bluetooth"' -DPLUGINDIR='"/home/jagomo/eclipse-workspace/bletest/src/bluez/plugins"' -DBLUETOOTH_PLUGIN_BUILTIN -I/usr/include/glib-2.0 -I../usr/include/gtk-2.0 -I/usr/include/glib-2.0/glib -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/dbus-1.0/dbus -I/usr/include/dbus-1.0 -I/usr/lib/x86_64-linux-gnu/dbus-1.0/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


