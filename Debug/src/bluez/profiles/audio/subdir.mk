################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/bluez/profiles/audio/a2dp.c \
../src/bluez/profiles/audio/avctp.c \
../src/bluez/profiles/audio/avdtp.c \
../src/bluez/profiles/audio/avrcp.c \
../src/bluez/profiles/audio/control.c \
../src/bluez/profiles/audio/media.c \
../src/bluez/profiles/audio/player.c \
../src/bluez/profiles/audio/sink.c \
../src/bluez/profiles/audio/source.c \
../src/bluez/profiles/audio/transport.c 

OBJS += \
./src/bluez/profiles/audio/a2dp.o \
./src/bluez/profiles/audio/avctp.o \
./src/bluez/profiles/audio/avdtp.o \
./src/bluez/profiles/audio/avrcp.o \
./src/bluez/profiles/audio/control.o \
./src/bluez/profiles/audio/media.o \
./src/bluez/profiles/audio/player.o \
./src/bluez/profiles/audio/sink.o \
./src/bluez/profiles/audio/source.o \
./src/bluez/profiles/audio/transport.o 

C_DEPS += \
./src/bluez/profiles/audio/a2dp.d \
./src/bluez/profiles/audio/avctp.d \
./src/bluez/profiles/audio/avdtp.d \
./src/bluez/profiles/audio/avrcp.d \
./src/bluez/profiles/audio/control.d \
./src/bluez/profiles/audio/media.d \
./src/bluez/profiles/audio/player.d \
./src/bluez/profiles/audio/sink.d \
./src/bluez/profiles/audio/source.d \
./src/bluez/profiles/audio/transport.d 


# Each subdirectory must supply rules for building sources it contributes
src/bluez/profiles/audio/%.o: ../src/bluez/profiles/audio/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -DVERSION='"DUMB"' -DSTORAGEDIR='"/usr/local/var/lib/bluetooth"' -D_GNU_SOURCE -DCONFIGDIR='"/usr/local/etc/bluetooth"' -DPLUGINDIR='"/home/jagomo/eclipse-workspace/bletest/src/bluez/plugins"' -DBLUETOOTH_PLUGIN_BUILTIN -I/usr/include/glib-2.0 -I../usr/include/gtk-2.0 -I/usr/include/glib-2.0/glib -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/dbus-1.0/dbus -I/usr/include/dbus-1.0 -I/usr/lib/x86_64-linux-gnu/dbus-1.0/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


