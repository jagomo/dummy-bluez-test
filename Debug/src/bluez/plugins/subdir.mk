################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/bluez/plugins/autopair.c \
../src/bluez/plugins/external-dummy.c \
../src/bluez/plugins/hostname.c \
../src/bluez/plugins/neard.c \
../src/bluez/plugins/policy.c \
../src/bluez/plugins/sixaxis.c \
../src/bluez/plugins/wiimote.c 

OBJS += \
./src/bluez/plugins/autopair.o \
./src/bluez/plugins/external-dummy.o \
./src/bluez/plugins/hostname.o \
./src/bluez/plugins/neard.o \
./src/bluez/plugins/policy.o \
./src/bluez/plugins/sixaxis.o \
./src/bluez/plugins/wiimote.o 

C_DEPS += \
./src/bluez/plugins/autopair.d \
./src/bluez/plugins/external-dummy.d \
./src/bluez/plugins/hostname.d \
./src/bluez/plugins/neard.d \
./src/bluez/plugins/policy.d \
./src/bluez/plugins/sixaxis.d \
./src/bluez/plugins/wiimote.d 


# Each subdirectory must supply rules for building sources it contributes
src/bluez/plugins/%.o: ../src/bluez/plugins/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -DVERSION='"DUMB"' -DSTORAGEDIR='"/usr/local/var/lib/bluetooth"' -D_GNU_SOURCE -DCONFIGDIR='"/usr/local/etc/bluetooth"' -DPLUGINDIR='"/home/jagomo/eclipse-workspace/bletest/src/bluez/plugins"' -DBLUETOOTH_PLUGIN_BUILTIN -I/usr/include/glib-2.0 -I../usr/include/gtk-2.0 -I/usr/include/glib-2.0/glib -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/dbus-1.0/dbus -I/usr/include/dbus-1.0 -I/usr/lib/x86_64-linux-gnu/dbus-1.0/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


