################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/bluez/android/client/haltest.c \
../src/bluez/android/client/history.c \
../src/bluez/android/client/if-audio.c \
../src/bluez/android/client/if-av-sink.c \
../src/bluez/android/client/if-av.c \
../src/bluez/android/client/if-bt.c \
../src/bluez/android/client/if-gatt.c \
../src/bluez/android/client/if-hf-client.c \
../src/bluez/android/client/if-hf.c \
../src/bluez/android/client/if-hh.c \
../src/bluez/android/client/if-hl.c \
../src/bluez/android/client/if-mce.c \
../src/bluez/android/client/if-pan.c \
../src/bluez/android/client/if-rc-ctrl.c \
../src/bluez/android/client/if-rc.c \
../src/bluez/android/client/if-sco.c \
../src/bluez/android/client/if-sock.c \
../src/bluez/android/client/pollhandler.c \
../src/bluez/android/client/tabcompletion.c \
../src/bluez/android/client/terminal.c 

OBJS += \
./src/bluez/android/client/haltest.o \
./src/bluez/android/client/history.o \
./src/bluez/android/client/if-audio.o \
./src/bluez/android/client/if-av-sink.o \
./src/bluez/android/client/if-av.o \
./src/bluez/android/client/if-bt.o \
./src/bluez/android/client/if-gatt.o \
./src/bluez/android/client/if-hf-client.o \
./src/bluez/android/client/if-hf.o \
./src/bluez/android/client/if-hh.o \
./src/bluez/android/client/if-hl.o \
./src/bluez/android/client/if-mce.o \
./src/bluez/android/client/if-pan.o \
./src/bluez/android/client/if-rc-ctrl.o \
./src/bluez/android/client/if-rc.o \
./src/bluez/android/client/if-sco.o \
./src/bluez/android/client/if-sock.o \
./src/bluez/android/client/pollhandler.o \
./src/bluez/android/client/tabcompletion.o \
./src/bluez/android/client/terminal.o 

C_DEPS += \
./src/bluez/android/client/haltest.d \
./src/bluez/android/client/history.d \
./src/bluez/android/client/if-audio.d \
./src/bluez/android/client/if-av-sink.d \
./src/bluez/android/client/if-av.d \
./src/bluez/android/client/if-bt.d \
./src/bluez/android/client/if-gatt.d \
./src/bluez/android/client/if-hf-client.d \
./src/bluez/android/client/if-hf.d \
./src/bluez/android/client/if-hh.d \
./src/bluez/android/client/if-hl.d \
./src/bluez/android/client/if-mce.d \
./src/bluez/android/client/if-pan.d \
./src/bluez/android/client/if-rc-ctrl.d \
./src/bluez/android/client/if-rc.d \
./src/bluez/android/client/if-sco.d \
./src/bluez/android/client/if-sock.d \
./src/bluez/android/client/pollhandler.d \
./src/bluez/android/client/tabcompletion.d \
./src/bluez/android/client/terminal.d 


# Each subdirectory must supply rules for building sources it contributes
src/bluez/android/client/%.o: ../src/bluez/android/client/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -DVERSION='"DUMB"' -DSTORAGEDIR='"/usr/local/var/lib/bluetooth"' -D_GNU_SOURCE -DCONFIGDIR='"/usr/local/etc/bluetooth"' -DPLUGINDIR='"/home/jagomo/eclipse-workspace/bletest/src/bluez/plugins"' -DBLUETOOTH_PLUGIN_BUILTIN -I/usr/include/glib-2.0 -I../usr/include/gtk-2.0 -I/usr/include/glib-2.0/glib -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/dbus-1.0/dbus -I/usr/include/dbus-1.0 -I/usr/lib/x86_64-linux-gnu/dbus-1.0/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


