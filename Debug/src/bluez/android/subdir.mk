################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/bluez/android/a2dp-sink.c \
../src/bluez/android/a2dp.c \
../src/bluez/android/avctp.c \
../src/bluez/android/avdtp.c \
../src/bluez/android/avdtptest.c \
../src/bluez/android/avrcp-lib.c \
../src/bluez/android/avrcp.c \
../src/bluez/android/bluetooth.c \
../src/bluez/android/bluetoothd-snoop.c \
../src/bluez/android/bluetoothd-wrapper.c \
../src/bluez/android/gatt.c \
../src/bluez/android/hal-a2dp-sink.c \
../src/bluez/android/hal-a2dp.c \
../src/bluez/android/hal-audio-aptx.c \
../src/bluez/android/hal-audio-sbc.c \
../src/bluez/android/hal-audio.c \
../src/bluez/android/hal-avrcp-ctrl.c \
../src/bluez/android/hal-avrcp.c \
../src/bluez/android/hal-bluetooth.c \
../src/bluez/android/hal-gatt.c \
../src/bluez/android/hal-handsfree-client.c \
../src/bluez/android/hal-handsfree.c \
../src/bluez/android/hal-health.c \
../src/bluez/android/hal-hidhost.c \
../src/bluez/android/hal-ipc.c \
../src/bluez/android/hal-map-client.c \
../src/bluez/android/hal-pan.c \
../src/bluez/android/hal-sco.c \
../src/bluez/android/hal-socket.c \
../src/bluez/android/hal-utils.c \
../src/bluez/android/handsfree-client.c \
../src/bluez/android/handsfree.c \
../src/bluez/android/health.c \
../src/bluez/android/hidhost.c \
../src/bluez/android/ipc-tester.c \
../src/bluez/android/ipc.c \
../src/bluez/android/log.c \
../src/bluez/android/main.c \
../src/bluez/android/map-client.c \
../src/bluez/android/pan.c \
../src/bluez/android/sco.c \
../src/bluez/android/socket.c \
../src/bluez/android/system-emulator.c \
../src/bluez/android/test-ipc.c \
../src/bluez/android/tester-a2dp.c \
../src/bluez/android/tester-avrcp.c \
../src/bluez/android/tester-bluetooth.c \
../src/bluez/android/tester-gatt.c \
../src/bluez/android/tester-hdp.c \
../src/bluez/android/tester-hidhost.c \
../src/bluez/android/tester-main.c \
../src/bluez/android/tester-map-client.c \
../src/bluez/android/tester-pan.c \
../src/bluez/android/tester-socket.c 

OBJS += \
./src/bluez/android/a2dp-sink.o \
./src/bluez/android/a2dp.o \
./src/bluez/android/avctp.o \
./src/bluez/android/avdtp.o \
./src/bluez/android/avdtptest.o \
./src/bluez/android/avrcp-lib.o \
./src/bluez/android/avrcp.o \
./src/bluez/android/bluetooth.o \
./src/bluez/android/bluetoothd-snoop.o \
./src/bluez/android/bluetoothd-wrapper.o \
./src/bluez/android/gatt.o \
./src/bluez/android/hal-a2dp-sink.o \
./src/bluez/android/hal-a2dp.o \
./src/bluez/android/hal-audio-aptx.o \
./src/bluez/android/hal-audio-sbc.o \
./src/bluez/android/hal-audio.o \
./src/bluez/android/hal-avrcp-ctrl.o \
./src/bluez/android/hal-avrcp.o \
./src/bluez/android/hal-bluetooth.o \
./src/bluez/android/hal-gatt.o \
./src/bluez/android/hal-handsfree-client.o \
./src/bluez/android/hal-handsfree.o \
./src/bluez/android/hal-health.o \
./src/bluez/android/hal-hidhost.o \
./src/bluez/android/hal-ipc.o \
./src/bluez/android/hal-map-client.o \
./src/bluez/android/hal-pan.o \
./src/bluez/android/hal-sco.o \
./src/bluez/android/hal-socket.o \
./src/bluez/android/hal-utils.o \
./src/bluez/android/handsfree-client.o \
./src/bluez/android/handsfree.o \
./src/bluez/android/health.o \
./src/bluez/android/hidhost.o \
./src/bluez/android/ipc-tester.o \
./src/bluez/android/ipc.o \
./src/bluez/android/log.o \
./src/bluez/android/main.o \
./src/bluez/android/map-client.o \
./src/bluez/android/pan.o \
./src/bluez/android/sco.o \
./src/bluez/android/socket.o \
./src/bluez/android/system-emulator.o \
./src/bluez/android/test-ipc.o \
./src/bluez/android/tester-a2dp.o \
./src/bluez/android/tester-avrcp.o \
./src/bluez/android/tester-bluetooth.o \
./src/bluez/android/tester-gatt.o \
./src/bluez/android/tester-hdp.o \
./src/bluez/android/tester-hidhost.o \
./src/bluez/android/tester-main.o \
./src/bluez/android/tester-map-client.o \
./src/bluez/android/tester-pan.o \
./src/bluez/android/tester-socket.o 

C_DEPS += \
./src/bluez/android/a2dp-sink.d \
./src/bluez/android/a2dp.d \
./src/bluez/android/avctp.d \
./src/bluez/android/avdtp.d \
./src/bluez/android/avdtptest.d \
./src/bluez/android/avrcp-lib.d \
./src/bluez/android/avrcp.d \
./src/bluez/android/bluetooth.d \
./src/bluez/android/bluetoothd-snoop.d \
./src/bluez/android/bluetoothd-wrapper.d \
./src/bluez/android/gatt.d \
./src/bluez/android/hal-a2dp-sink.d \
./src/bluez/android/hal-a2dp.d \
./src/bluez/android/hal-audio-aptx.d \
./src/bluez/android/hal-audio-sbc.d \
./src/bluez/android/hal-audio.d \
./src/bluez/android/hal-avrcp-ctrl.d \
./src/bluez/android/hal-avrcp.d \
./src/bluez/android/hal-bluetooth.d \
./src/bluez/android/hal-gatt.d \
./src/bluez/android/hal-handsfree-client.d \
./src/bluez/android/hal-handsfree.d \
./src/bluez/android/hal-health.d \
./src/bluez/android/hal-hidhost.d \
./src/bluez/android/hal-ipc.d \
./src/bluez/android/hal-map-client.d \
./src/bluez/android/hal-pan.d \
./src/bluez/android/hal-sco.d \
./src/bluez/android/hal-socket.d \
./src/bluez/android/hal-utils.d \
./src/bluez/android/handsfree-client.d \
./src/bluez/android/handsfree.d \
./src/bluez/android/health.d \
./src/bluez/android/hidhost.d \
./src/bluez/android/ipc-tester.d \
./src/bluez/android/ipc.d \
./src/bluez/android/log.d \
./src/bluez/android/main.d \
./src/bluez/android/map-client.d \
./src/bluez/android/pan.d \
./src/bluez/android/sco.d \
./src/bluez/android/socket.d \
./src/bluez/android/system-emulator.d \
./src/bluez/android/test-ipc.d \
./src/bluez/android/tester-a2dp.d \
./src/bluez/android/tester-avrcp.d \
./src/bluez/android/tester-bluetooth.d \
./src/bluez/android/tester-gatt.d \
./src/bluez/android/tester-hdp.d \
./src/bluez/android/tester-hidhost.d \
./src/bluez/android/tester-main.d \
./src/bluez/android/tester-map-client.d \
./src/bluez/android/tester-pan.d \
./src/bluez/android/tester-socket.d 


# Each subdirectory must supply rules for building sources it contributes
src/bluez/android/%.o: ../src/bluez/android/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -DVERSION='"DUMB"' -DSTORAGEDIR='"/usr/local/var/lib/bluetooth"' -D_GNU_SOURCE -DCONFIGDIR='"/usr/local/etc/bluetooth"' -DPLUGINDIR='"/home/jagomo/eclipse-workspace/bletest/src/bluez/plugins"' -DBLUETOOTH_PLUGIN_BUILTIN -I/usr/include/glib-2.0 -I../usr/include/gtk-2.0 -I/usr/include/glib-2.0/glib -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/dbus-1.0/dbus -I/usr/include/dbus-1.0 -I/usr/lib/x86_64-linux-gnu/dbus-1.0/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


