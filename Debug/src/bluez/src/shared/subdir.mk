################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/bluez/src/shared/ad.c \
../src/bluez/src/shared/att.c \
../src/bluez/src/shared/btsnoop.c \
../src/bluez/src/shared/crypto.c \
../src/bluez/src/shared/ecc.c \
../src/bluez/src/shared/gap.c \
../src/bluez/src/shared/gatt-client.c \
../src/bluez/src/shared/gatt-db.c \
../src/bluez/src/shared/gatt-helpers.c \
../src/bluez/src/shared/gatt-server.c \
../src/bluez/src/shared/hci-crypto.c \
../src/bluez/src/shared/hci.c \
../src/bluez/src/shared/hfp.c \
../src/bluez/src/shared/io-glib.c \
../src/bluez/src/shared/io-mainloop.c \
../src/bluez/src/shared/mainloop.c \
../src/bluez/src/shared/mgmt.c \
../src/bluez/src/shared/pcap.c \
../src/bluez/src/shared/queue.c \
../src/bluez/src/shared/ringbuf.c \
../src/bluez/src/shared/shell.c \
../src/bluez/src/shared/tester.c \
../src/bluez/src/shared/timeout-glib.c \
../src/bluez/src/shared/timeout-mainloop.c \
../src/bluez/src/shared/uhid.c \
../src/bluez/src/shared/util.c 

OBJS += \
./src/bluez/src/shared/ad.o \
./src/bluez/src/shared/att.o \
./src/bluez/src/shared/btsnoop.o \
./src/bluez/src/shared/crypto.o \
./src/bluez/src/shared/ecc.o \
./src/bluez/src/shared/gap.o \
./src/bluez/src/shared/gatt-client.o \
./src/bluez/src/shared/gatt-db.o \
./src/bluez/src/shared/gatt-helpers.o \
./src/bluez/src/shared/gatt-server.o \
./src/bluez/src/shared/hci-crypto.o \
./src/bluez/src/shared/hci.o \
./src/bluez/src/shared/hfp.o \
./src/bluez/src/shared/io-glib.o \
./src/bluez/src/shared/io-mainloop.o \
./src/bluez/src/shared/mainloop.o \
./src/bluez/src/shared/mgmt.o \
./src/bluez/src/shared/pcap.o \
./src/bluez/src/shared/queue.o \
./src/bluez/src/shared/ringbuf.o \
./src/bluez/src/shared/shell.o \
./src/bluez/src/shared/tester.o \
./src/bluez/src/shared/timeout-glib.o \
./src/bluez/src/shared/timeout-mainloop.o \
./src/bluez/src/shared/uhid.o \
./src/bluez/src/shared/util.o 

C_DEPS += \
./src/bluez/src/shared/ad.d \
./src/bluez/src/shared/att.d \
./src/bluez/src/shared/btsnoop.d \
./src/bluez/src/shared/crypto.d \
./src/bluez/src/shared/ecc.d \
./src/bluez/src/shared/gap.d \
./src/bluez/src/shared/gatt-client.d \
./src/bluez/src/shared/gatt-db.d \
./src/bluez/src/shared/gatt-helpers.d \
./src/bluez/src/shared/gatt-server.d \
./src/bluez/src/shared/hci-crypto.d \
./src/bluez/src/shared/hci.d \
./src/bluez/src/shared/hfp.d \
./src/bluez/src/shared/io-glib.d \
./src/bluez/src/shared/io-mainloop.d \
./src/bluez/src/shared/mainloop.d \
./src/bluez/src/shared/mgmt.d \
./src/bluez/src/shared/pcap.d \
./src/bluez/src/shared/queue.d \
./src/bluez/src/shared/ringbuf.d \
./src/bluez/src/shared/shell.d \
./src/bluez/src/shared/tester.d \
./src/bluez/src/shared/timeout-glib.d \
./src/bluez/src/shared/timeout-mainloop.d \
./src/bluez/src/shared/uhid.d \
./src/bluez/src/shared/util.d 


# Each subdirectory must supply rules for building sources it contributes
src/bluez/src/shared/%.o: ../src/bluez/src/shared/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -DVERSION='"DUMB"' -DSTORAGEDIR='"/usr/local/var/lib/bluetooth"' -D_GNU_SOURCE -DCONFIGDIR='"/usr/local/etc/bluetooth"' -DPLUGINDIR='"/home/jagomo/eclipse-workspace/bletest/src/bluez/plugins"' -DBLUETOOTH_PLUGIN_BUILTIN -I/usr/include/glib-2.0 -I../usr/include/gtk-2.0 -I/usr/include/glib-2.0/glib -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/dbus-1.0/dbus -I/usr/include/dbus-1.0 -I/usr/lib/x86_64-linux-gnu/dbus-1.0/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


