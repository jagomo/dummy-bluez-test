################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/bluez/src/advertising.c \
../src/bluez/src/agent.c \
../src/bluez/src/attrib-server.c \
../src/bluez/src/backtrace.c \
../src/bluez/src/dbus-common.c \
../src/bluez/src/device.c \
../src/bluez/src/eir.c \
../src/bluez/src/error.c \
../src/bluez/src/gatt-client.c \
../src/bluez/src/gatt-database.c \
../src/bluez/src/log.c \
../src/bluez/src/main.c \
../src/bluez/src/oui.c \
../src/bluez/src/plugin.c \
../src/bluez/src/profile.c \
../src/bluez/src/rfkill.c \
../src/bluez/src/sdp-client.c \
../src/bluez/src/sdp-xml.c \
../src/bluez/src/sdpd-database.c \
../src/bluez/src/sdpd-request.c \
../src/bluez/src/sdpd-server.c \
../src/bluez/src/sdpd-service.c \
../src/bluez/src/service.c \
../src/bluez/src/storage.c \
../src/bluez/src/systemd.c \
../src/bluez/src/textfile.c \
../src/bluez/src/uuid-helper.c 

OBJS += \
./src/bluez/src/advertising.o \
./src/bluez/src/agent.o \
./src/bluez/src/attrib-server.o \
./src/bluez/src/backtrace.o \
./src/bluez/src/dbus-common.o \
./src/bluez/src/device.o \
./src/bluez/src/eir.o \
./src/bluez/src/error.o \
./src/bluez/src/gatt-client.o \
./src/bluez/src/gatt-database.o \
./src/bluez/src/log.o \
./src/bluez/src/main.o \
./src/bluez/src/oui.o \
./src/bluez/src/plugin.o \
./src/bluez/src/profile.o \
./src/bluez/src/rfkill.o \
./src/bluez/src/sdp-client.o \
./src/bluez/src/sdp-xml.o \
./src/bluez/src/sdpd-database.o \
./src/bluez/src/sdpd-request.o \
./src/bluez/src/sdpd-server.o \
./src/bluez/src/sdpd-service.o \
./src/bluez/src/service.o \
./src/bluez/src/storage.o \
./src/bluez/src/systemd.o \
./src/bluez/src/textfile.o \
./src/bluez/src/uuid-helper.o 

C_DEPS += \
./src/bluez/src/advertising.d \
./src/bluez/src/agent.d \
./src/bluez/src/attrib-server.d \
./src/bluez/src/backtrace.d \
./src/bluez/src/dbus-common.d \
./src/bluez/src/device.d \
./src/bluez/src/eir.d \
./src/bluez/src/error.d \
./src/bluez/src/gatt-client.d \
./src/bluez/src/gatt-database.d \
./src/bluez/src/log.d \
./src/bluez/src/main.d \
./src/bluez/src/oui.d \
./src/bluez/src/plugin.d \
./src/bluez/src/profile.d \
./src/bluez/src/rfkill.d \
./src/bluez/src/sdp-client.d \
./src/bluez/src/sdp-xml.d \
./src/bluez/src/sdpd-database.d \
./src/bluez/src/sdpd-request.d \
./src/bluez/src/sdpd-server.d \
./src/bluez/src/sdpd-service.d \
./src/bluez/src/service.d \
./src/bluez/src/storage.d \
./src/bluez/src/systemd.d \
./src/bluez/src/textfile.d \
./src/bluez/src/uuid-helper.d 


# Each subdirectory must supply rules for building sources it contributes
src/bluez/src/%.o: ../src/bluez/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -DVERSION='"DUMB"' -DSTORAGEDIR='"/usr/local/var/lib/bluetooth"' -D_GNU_SOURCE -DCONFIGDIR='"/usr/local/etc/bluetooth"' -DPLUGINDIR='"/home/jagomo/eclipse-workspace/bletest/src/bluez/plugins"' -DBLUETOOTH_PLUGIN_BUILTIN -I/usr/include/glib-2.0 -I../usr/include/gtk-2.0 -I/usr/include/glib-2.0/glib -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/dbus-1.0/dbus -I/usr/include/dbus-1.0 -I/usr/lib/x86_64-linux-gnu/dbus-1.0/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


