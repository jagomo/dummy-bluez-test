################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/bluez/gdbus/client.c \
../src/bluez/gdbus/mainloop.c \
../src/bluez/gdbus/object.c \
../src/bluez/gdbus/polkit.c \
../src/bluez/gdbus/watch.c 

OBJS += \
./src/bluez/gdbus/client.o \
./src/bluez/gdbus/mainloop.o \
./src/bluez/gdbus/object.o \
./src/bluez/gdbus/polkit.o \
./src/bluez/gdbus/watch.o 

C_DEPS += \
./src/bluez/gdbus/client.d \
./src/bluez/gdbus/mainloop.d \
./src/bluez/gdbus/object.d \
./src/bluez/gdbus/polkit.d \
./src/bluez/gdbus/watch.d 


# Each subdirectory must supply rules for building sources it contributes
src/bluez/gdbus/%.o: ../src/bluez/gdbus/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -DVERSION='"DUMB"' -DSTORAGEDIR='"/usr/local/var/lib/bluetooth"' -D_GNU_SOURCE -DCONFIGDIR='"/usr/local/etc/bluetooth"' -DPLUGINDIR='"/home/jagomo/eclipse-workspace/bletest/src/bluez/plugins"' -DBLUETOOTH_PLUGIN_BUILTIN -I/usr/include/glib-2.0 -I../usr/include/gtk-2.0 -I/usr/include/glib-2.0/glib -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/dbus-1.0/dbus -I/usr/include/dbus-1.0 -I/usr/lib/x86_64-linux-gnu/dbus-1.0/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


