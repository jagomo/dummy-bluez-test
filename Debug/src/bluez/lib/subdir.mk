################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/bluez/lib/bluetooth.c \
../src/bluez/lib/hci.c \
../src/bluez/lib/sdp.c \
../src/bluez/lib/uuid.c 

OBJS += \
./src/bluez/lib/bluetooth.o \
./src/bluez/lib/hci.o \
./src/bluez/lib/sdp.o \
./src/bluez/lib/uuid.o 

C_DEPS += \
./src/bluez/lib/bluetooth.d \
./src/bluez/lib/hci.d \
./src/bluez/lib/sdp.d \
./src/bluez/lib/uuid.d 


# Each subdirectory must supply rules for building sources it contributes
src/bluez/lib/%.o: ../src/bluez/lib/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -DVERSION='"DUMB"' -DSTORAGEDIR='"/usr/local/var/lib/bluetooth"' -D_GNU_SOURCE -DCONFIGDIR='"/usr/local/etc/bluetooth"' -DPLUGINDIR='"/home/jagomo/eclipse-workspace/bletest/src/bluez/plugins"' -DBLUETOOTH_PLUGIN_BUILTIN -I/usr/include/glib-2.0 -I../usr/include/gtk-2.0 -I/usr/include/glib-2.0/glib -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/dbus-1.0/dbus -I/usr/include/dbus-1.0 -I/usr/lib/x86_64-linux-gnu/dbus-1.0/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


