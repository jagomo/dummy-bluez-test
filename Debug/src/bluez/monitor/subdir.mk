################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/bluez/monitor/a2dp.c \
../src/bluez/monitor/analyze.c \
../src/bluez/monitor/avctp.c \
../src/bluez/monitor/avdtp.c \
../src/bluez/monitor/bnep.c \
../src/bluez/monitor/broadcom.c \
../src/bluez/monitor/control.c \
../src/bluez/monitor/crc.c \
../src/bluez/monitor/display.c \
../src/bluez/monitor/ellisys.c \
../src/bluez/monitor/hcidump.c \
../src/bluez/monitor/hwdb.c \
../src/bluez/monitor/intel.c \
../src/bluez/monitor/keys.c \
../src/bluez/monitor/l2cap.c \
../src/bluez/monitor/ll.c \
../src/bluez/monitor/lmp.c \
../src/bluez/monitor/main.c \
../src/bluez/monitor/packet.c \
../src/bluez/monitor/rfcomm.c \
../src/bluez/monitor/sdp.c \
../src/bluez/monitor/vendor.c 

OBJS += \
./src/bluez/monitor/a2dp.o \
./src/bluez/monitor/analyze.o \
./src/bluez/monitor/avctp.o \
./src/bluez/monitor/avdtp.o \
./src/bluez/monitor/bnep.o \
./src/bluez/monitor/broadcom.o \
./src/bluez/monitor/control.o \
./src/bluez/monitor/crc.o \
./src/bluez/monitor/display.o \
./src/bluez/monitor/ellisys.o \
./src/bluez/monitor/hcidump.o \
./src/bluez/monitor/hwdb.o \
./src/bluez/monitor/intel.o \
./src/bluez/monitor/keys.o \
./src/bluez/monitor/l2cap.o \
./src/bluez/monitor/ll.o \
./src/bluez/monitor/lmp.o \
./src/bluez/monitor/main.o \
./src/bluez/monitor/packet.o \
./src/bluez/monitor/rfcomm.o \
./src/bluez/monitor/sdp.o \
./src/bluez/monitor/vendor.o 

C_DEPS += \
./src/bluez/monitor/a2dp.d \
./src/bluez/monitor/analyze.d \
./src/bluez/monitor/avctp.d \
./src/bluez/monitor/avdtp.d \
./src/bluez/monitor/bnep.d \
./src/bluez/monitor/broadcom.d \
./src/bluez/monitor/control.d \
./src/bluez/monitor/crc.d \
./src/bluez/monitor/display.d \
./src/bluez/monitor/ellisys.d \
./src/bluez/monitor/hcidump.d \
./src/bluez/monitor/hwdb.d \
./src/bluez/monitor/intel.d \
./src/bluez/monitor/keys.d \
./src/bluez/monitor/l2cap.d \
./src/bluez/monitor/ll.d \
./src/bluez/monitor/lmp.d \
./src/bluez/monitor/main.d \
./src/bluez/monitor/packet.d \
./src/bluez/monitor/rfcomm.d \
./src/bluez/monitor/sdp.d \
./src/bluez/monitor/vendor.d 


# Each subdirectory must supply rules for building sources it contributes
src/bluez/monitor/%.o: ../src/bluez/monitor/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -DVERSION='"DUMB"' -DSTORAGEDIR='"/usr/local/var/lib/bluetooth"' -D_GNU_SOURCE -DCONFIGDIR='"/usr/local/etc/bluetooth"' -DPLUGINDIR='"/home/jagomo/eclipse-workspace/bletest/src/bluez/plugins"' -DBLUETOOTH_PLUGIN_BUILTIN -I/usr/include/glib-2.0 -I../usr/include/gtk-2.0 -I/usr/include/glib-2.0/glib -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/dbus-1.0/dbus -I/usr/include/dbus-1.0 -I/usr/lib/x86_64-linux-gnu/dbus-1.0/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


