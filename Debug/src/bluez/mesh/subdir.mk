################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/bluez/mesh/agent.c \
../src/bluez/mesh/config-client.c \
../src/bluez/mesh/config-server.c \
../src/bluez/mesh/crypto.c \
../src/bluez/mesh/gatt.c \
../src/bluez/mesh/main.c \
../src/bluez/mesh/net.c \
../src/bluez/mesh/node.c \
../src/bluez/mesh/onoff-model.c \
../src/bluez/mesh/prov-db.c \
../src/bluez/mesh/prov.c \
../src/bluez/mesh/util.c 

OBJS += \
./src/bluez/mesh/agent.o \
./src/bluez/mesh/config-client.o \
./src/bluez/mesh/config-server.o \
./src/bluez/mesh/crypto.o \
./src/bluez/mesh/gatt.o \
./src/bluez/mesh/main.o \
./src/bluez/mesh/net.o \
./src/bluez/mesh/node.o \
./src/bluez/mesh/onoff-model.o \
./src/bluez/mesh/prov-db.o \
./src/bluez/mesh/prov.o \
./src/bluez/mesh/util.o 

C_DEPS += \
./src/bluez/mesh/agent.d \
./src/bluez/mesh/config-client.d \
./src/bluez/mesh/config-server.d \
./src/bluez/mesh/crypto.d \
./src/bluez/mesh/gatt.d \
./src/bluez/mesh/main.d \
./src/bluez/mesh/net.d \
./src/bluez/mesh/node.d \
./src/bluez/mesh/onoff-model.d \
./src/bluez/mesh/prov-db.d \
./src/bluez/mesh/prov.d \
./src/bluez/mesh/util.d 


# Each subdirectory must supply rules for building sources it contributes
src/bluez/mesh/%.o: ../src/bluez/mesh/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -DVERSION='"DUMB"' -DSTORAGEDIR='"/usr/local/var/lib/bluetooth"' -D_GNU_SOURCE -DCONFIGDIR='"/usr/local/etc/bluetooth"' -DPLUGINDIR='"/home/jagomo/eclipse-workspace/bletest/src/bluez/plugins"' -DBLUETOOTH_PLUGIN_BUILTIN -I/usr/include/glib-2.0 -I../usr/include/gtk-2.0 -I/usr/include/glib-2.0/glib -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/dbus-1.0/dbus -I/usr/include/dbus-1.0 -I/usr/lib/x86_64-linux-gnu/dbus-1.0/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


