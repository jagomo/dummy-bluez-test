################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/bluez/obexd/plugins/bluetooth.c \
../src/bluez/obexd/plugins/filesystem.c \
../src/bluez/obexd/plugins/ftp.c \
../src/bluez/obexd/plugins/irmc.c \
../src/bluez/obexd/plugins/mas.c \
../src/bluez/obexd/plugins/messages-dummy.c \
../src/bluez/obexd/plugins/opp.c \
../src/bluez/obexd/plugins/pbap.c \
../src/bluez/obexd/plugins/pcsuite.c \
../src/bluez/obexd/plugins/phonebook-dummy.c \
../src/bluez/obexd/plugins/vcard.c 

OBJS += \
./src/bluez/obexd/plugins/bluetooth.o \
./src/bluez/obexd/plugins/filesystem.o \
./src/bluez/obexd/plugins/ftp.o \
./src/bluez/obexd/plugins/irmc.o \
./src/bluez/obexd/plugins/mas.o \
./src/bluez/obexd/plugins/messages-dummy.o \
./src/bluez/obexd/plugins/opp.o \
./src/bluez/obexd/plugins/pbap.o \
./src/bluez/obexd/plugins/pcsuite.o \
./src/bluez/obexd/plugins/phonebook-dummy.o \
./src/bluez/obexd/plugins/vcard.o 

C_DEPS += \
./src/bluez/obexd/plugins/bluetooth.d \
./src/bluez/obexd/plugins/filesystem.d \
./src/bluez/obexd/plugins/ftp.d \
./src/bluez/obexd/plugins/irmc.d \
./src/bluez/obexd/plugins/mas.d \
./src/bluez/obexd/plugins/messages-dummy.d \
./src/bluez/obexd/plugins/opp.d \
./src/bluez/obexd/plugins/pbap.d \
./src/bluez/obexd/plugins/pcsuite.d \
./src/bluez/obexd/plugins/phonebook-dummy.d \
./src/bluez/obexd/plugins/vcard.d 


# Each subdirectory must supply rules for building sources it contributes
src/bluez/obexd/plugins/%.o: ../src/bluez/obexd/plugins/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -DVERSION='"DUMB"' -DSTORAGEDIR='"/usr/local/var/lib/bluetooth"' -D_GNU_SOURCE -DCONFIGDIR='"/usr/local/etc/bluetooth"' -DPLUGINDIR='"/home/jagomo/eclipse-workspace/bletest/src/bluez/plugins"' -DBLUETOOTH_PLUGIN_BUILTIN -I/usr/include/glib-2.0 -I../usr/include/gtk-2.0 -I/usr/include/glib-2.0/glib -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/dbus-1.0/dbus -I/usr/include/dbus-1.0 -I/usr/lib/x86_64-linux-gnu/dbus-1.0/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


