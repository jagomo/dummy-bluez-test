################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/bluez/obexd/src/log.c \
../src/bluez/obexd/src/main.c \
../src/bluez/obexd/src/manager.c \
../src/bluez/obexd/src/mimetype.c \
../src/bluez/obexd/src/obex.c \
../src/bluez/obexd/src/plugin.c \
../src/bluez/obexd/src/server.c \
../src/bluez/obexd/src/service.c \
../src/bluez/obexd/src/transport.c 

OBJS += \
./src/bluez/obexd/src/log.o \
./src/bluez/obexd/src/main.o \
./src/bluez/obexd/src/manager.o \
./src/bluez/obexd/src/mimetype.o \
./src/bluez/obexd/src/obex.o \
./src/bluez/obexd/src/plugin.o \
./src/bluez/obexd/src/server.o \
./src/bluez/obexd/src/service.o \
./src/bluez/obexd/src/transport.o 

C_DEPS += \
./src/bluez/obexd/src/log.d \
./src/bluez/obexd/src/main.d \
./src/bluez/obexd/src/manager.d \
./src/bluez/obexd/src/mimetype.d \
./src/bluez/obexd/src/obex.d \
./src/bluez/obexd/src/plugin.d \
./src/bluez/obexd/src/server.d \
./src/bluez/obexd/src/service.d \
./src/bluez/obexd/src/transport.d 


# Each subdirectory must supply rules for building sources it contributes
src/bluez/obexd/src/%.o: ../src/bluez/obexd/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -DVERSION='"DUMB"' -DSTORAGEDIR='"/usr/local/var/lib/bluetooth"' -D_GNU_SOURCE -DCONFIGDIR='"/usr/local/etc/bluetooth"' -DPLUGINDIR='"/home/jagomo/eclipse-workspace/bletest/src/bluez/plugins"' -DBLUETOOTH_PLUGIN_BUILTIN -I/usr/include/glib-2.0 -I../usr/include/gtk-2.0 -I/usr/include/glib-2.0/glib -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/dbus-1.0/dbus -I/usr/include/dbus-1.0 -I/usr/lib/x86_64-linux-gnu/dbus-1.0/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


