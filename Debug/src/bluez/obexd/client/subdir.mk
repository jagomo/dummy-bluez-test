################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/bluez/obexd/client/bluetooth.c \
../src/bluez/obexd/client/dbus.c \
../src/bluez/obexd/client/driver.c \
../src/bluez/obexd/client/ftp.c \
../src/bluez/obexd/client/manager.c \
../src/bluez/obexd/client/map-event.c \
../src/bluez/obexd/client/map.c \
../src/bluez/obexd/client/mns.c \
../src/bluez/obexd/client/opp.c \
../src/bluez/obexd/client/pbap.c \
../src/bluez/obexd/client/session.c \
../src/bluez/obexd/client/sync.c \
../src/bluez/obexd/client/transfer.c \
../src/bluez/obexd/client/transport.c 

OBJS += \
./src/bluez/obexd/client/bluetooth.o \
./src/bluez/obexd/client/dbus.o \
./src/bluez/obexd/client/driver.o \
./src/bluez/obexd/client/ftp.o \
./src/bluez/obexd/client/manager.o \
./src/bluez/obexd/client/map-event.o \
./src/bluez/obexd/client/map.o \
./src/bluez/obexd/client/mns.o \
./src/bluez/obexd/client/opp.o \
./src/bluez/obexd/client/pbap.o \
./src/bluez/obexd/client/session.o \
./src/bluez/obexd/client/sync.o \
./src/bluez/obexd/client/transfer.o \
./src/bluez/obexd/client/transport.o 

C_DEPS += \
./src/bluez/obexd/client/bluetooth.d \
./src/bluez/obexd/client/dbus.d \
./src/bluez/obexd/client/driver.d \
./src/bluez/obexd/client/ftp.d \
./src/bluez/obexd/client/manager.d \
./src/bluez/obexd/client/map-event.d \
./src/bluez/obexd/client/map.d \
./src/bluez/obexd/client/mns.d \
./src/bluez/obexd/client/opp.d \
./src/bluez/obexd/client/pbap.d \
./src/bluez/obexd/client/session.d \
./src/bluez/obexd/client/sync.d \
./src/bluez/obexd/client/transfer.d \
./src/bluez/obexd/client/transport.d 


# Each subdirectory must supply rules for building sources it contributes
src/bluez/obexd/client/%.o: ../src/bluez/obexd/client/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -DVERSION='"DUMB"' -DSTORAGEDIR='"/usr/local/var/lib/bluetooth"' -D_GNU_SOURCE -DCONFIGDIR='"/usr/local/etc/bluetooth"' -DPLUGINDIR='"/home/jagomo/eclipse-workspace/bletest/src/bluez/plugins"' -DBLUETOOTH_PLUGIN_BUILTIN -I/usr/include/glib-2.0 -I../usr/include/gtk-2.0 -I/usr/include/glib-2.0/glib -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/dbus-1.0/dbus -I/usr/include/dbus-1.0 -I/usr/lib/x86_64-linux-gnu/dbus-1.0/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


