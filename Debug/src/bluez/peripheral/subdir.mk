################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/bluez/peripheral/attach.c \
../src/bluez/peripheral/efivars.c \
../src/bluez/peripheral/gap.c \
../src/bluez/peripheral/gatt.c \
../src/bluez/peripheral/log.c \
../src/bluez/peripheral/main.c 

OBJS += \
./src/bluez/peripheral/attach.o \
./src/bluez/peripheral/efivars.o \
./src/bluez/peripheral/gap.o \
./src/bluez/peripheral/gatt.o \
./src/bluez/peripheral/log.o \
./src/bluez/peripheral/main.o 

C_DEPS += \
./src/bluez/peripheral/attach.d \
./src/bluez/peripheral/efivars.d \
./src/bluez/peripheral/gap.d \
./src/bluez/peripheral/gatt.d \
./src/bluez/peripheral/log.d \
./src/bluez/peripheral/main.d 


# Each subdirectory must supply rules for building sources it contributes
src/bluez/peripheral/%.o: ../src/bluez/peripheral/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -DVERSION='"DUMB"' -DSTORAGEDIR='"/usr/local/var/lib/bluetooth"' -D_GNU_SOURCE -DCONFIGDIR='"/usr/local/etc/bluetooth"' -DPLUGINDIR='"/home/jagomo/eclipse-workspace/bletest/src/bluez/plugins"' -DBLUETOOTH_PLUGIN_BUILTIN -I/usr/include/glib-2.0 -I../usr/include/gtk-2.0 -I/usr/include/glib-2.0/glib -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/dbus-1.0/dbus -I/usr/include/dbus-1.0 -I/usr/lib/x86_64-linux-gnu/dbus-1.0/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


