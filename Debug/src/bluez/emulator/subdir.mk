################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/bluez/emulator/amp.c \
../src/bluez/emulator/b1ee.c \
../src/bluez/emulator/btdev.c \
../src/bluez/emulator/bthost.c \
../src/bluez/emulator/hciemu.c \
../src/bluez/emulator/hfp.c \
../src/bluez/emulator/le.c \
../src/bluez/emulator/main.c \
../src/bluez/emulator/phy.c \
../src/bluez/emulator/serial.c \
../src/bluez/emulator/server.c \
../src/bluez/emulator/smp.c \
../src/bluez/emulator/vhci.c 

OBJS += \
./src/bluez/emulator/amp.o \
./src/bluez/emulator/b1ee.o \
./src/bluez/emulator/btdev.o \
./src/bluez/emulator/bthost.o \
./src/bluez/emulator/hciemu.o \
./src/bluez/emulator/hfp.o \
./src/bluez/emulator/le.o \
./src/bluez/emulator/main.o \
./src/bluez/emulator/phy.o \
./src/bluez/emulator/serial.o \
./src/bluez/emulator/server.o \
./src/bluez/emulator/smp.o \
./src/bluez/emulator/vhci.o 

C_DEPS += \
./src/bluez/emulator/amp.d \
./src/bluez/emulator/b1ee.d \
./src/bluez/emulator/btdev.d \
./src/bluez/emulator/bthost.d \
./src/bluez/emulator/hciemu.d \
./src/bluez/emulator/hfp.d \
./src/bluez/emulator/le.d \
./src/bluez/emulator/main.d \
./src/bluez/emulator/phy.d \
./src/bluez/emulator/serial.d \
./src/bluez/emulator/server.d \
./src/bluez/emulator/smp.d \
./src/bluez/emulator/vhci.d 


# Each subdirectory must supply rules for building sources it contributes
src/bluez/emulator/%.o: ../src/bluez/emulator/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -DVERSION='"DUMB"' -DSTORAGEDIR='"/usr/local/var/lib/bluetooth"' -D_GNU_SOURCE -DCONFIGDIR='"/usr/local/etc/bluetooth"' -DPLUGINDIR='"/home/jagomo/eclipse-workspace/bletest/src/bluez/plugins"' -DBLUETOOTH_PLUGIN_BUILTIN -I/usr/include/glib-2.0 -I../usr/include/gtk-2.0 -I/usr/include/glib-2.0/glib -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/dbus-1.0/dbus -I/usr/include/dbus-1.0 -I/usr/lib/x86_64-linux-gnu/dbus-1.0/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


