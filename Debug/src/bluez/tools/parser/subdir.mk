################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/bluez/tools/parser/amp.c \
../src/bluez/tools/parser/att.c \
../src/bluez/tools/parser/avctp.c \
../src/bluez/tools/parser/avdtp.c \
../src/bluez/tools/parser/avrcp.c \
../src/bluez/tools/parser/bnep.c \
../src/bluez/tools/parser/bpa.c \
../src/bluez/tools/parser/capi.c \
../src/bluez/tools/parser/cmtp.c \
../src/bluez/tools/parser/csr.c \
../src/bluez/tools/parser/ericsson.c \
../src/bluez/tools/parser/hci.c \
../src/bluez/tools/parser/hcrp.c \
../src/bluez/tools/parser/hidp.c \
../src/bluez/tools/parser/l2cap.c \
../src/bluez/tools/parser/lmp.c \
../src/bluez/tools/parser/obex.c \
../src/bluez/tools/parser/parser.c \
../src/bluez/tools/parser/ppp.c \
../src/bluez/tools/parser/rfcomm.c \
../src/bluez/tools/parser/sap.c \
../src/bluez/tools/parser/sdp.c \
../src/bluez/tools/parser/smp.c \
../src/bluez/tools/parser/tcpip.c 

OBJS += \
./src/bluez/tools/parser/amp.o \
./src/bluez/tools/parser/att.o \
./src/bluez/tools/parser/avctp.o \
./src/bluez/tools/parser/avdtp.o \
./src/bluez/tools/parser/avrcp.o \
./src/bluez/tools/parser/bnep.o \
./src/bluez/tools/parser/bpa.o \
./src/bluez/tools/parser/capi.o \
./src/bluez/tools/parser/cmtp.o \
./src/bluez/tools/parser/csr.o \
./src/bluez/tools/parser/ericsson.o \
./src/bluez/tools/parser/hci.o \
./src/bluez/tools/parser/hcrp.o \
./src/bluez/tools/parser/hidp.o \
./src/bluez/tools/parser/l2cap.o \
./src/bluez/tools/parser/lmp.o \
./src/bluez/tools/parser/obex.o \
./src/bluez/tools/parser/parser.o \
./src/bluez/tools/parser/ppp.o \
./src/bluez/tools/parser/rfcomm.o \
./src/bluez/tools/parser/sap.o \
./src/bluez/tools/parser/sdp.o \
./src/bluez/tools/parser/smp.o \
./src/bluez/tools/parser/tcpip.o 

C_DEPS += \
./src/bluez/tools/parser/amp.d \
./src/bluez/tools/parser/att.d \
./src/bluez/tools/parser/avctp.d \
./src/bluez/tools/parser/avdtp.d \
./src/bluez/tools/parser/avrcp.d \
./src/bluez/tools/parser/bnep.d \
./src/bluez/tools/parser/bpa.d \
./src/bluez/tools/parser/capi.d \
./src/bluez/tools/parser/cmtp.d \
./src/bluez/tools/parser/csr.d \
./src/bluez/tools/parser/ericsson.d \
./src/bluez/tools/parser/hci.d \
./src/bluez/tools/parser/hcrp.d \
./src/bluez/tools/parser/hidp.d \
./src/bluez/tools/parser/l2cap.d \
./src/bluez/tools/parser/lmp.d \
./src/bluez/tools/parser/obex.d \
./src/bluez/tools/parser/parser.d \
./src/bluez/tools/parser/ppp.d \
./src/bluez/tools/parser/rfcomm.d \
./src/bluez/tools/parser/sap.d \
./src/bluez/tools/parser/sdp.d \
./src/bluez/tools/parser/smp.d \
./src/bluez/tools/parser/tcpip.d 


# Each subdirectory must supply rules for building sources it contributes
src/bluez/tools/parser/%.o: ../src/bluez/tools/parser/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C Compiler'
	gcc -DVERSION='"DUMB"' -DSTORAGEDIR='"/usr/local/var/lib/bluetooth"' -D_GNU_SOURCE -DCONFIGDIR='"/usr/local/etc/bluetooth"' -DPLUGINDIR='"/home/jagomo/eclipse-workspace/bletest/src/bluez/plugins"' -DBLUETOOTH_PLUGIN_BUILTIN -I/usr/include/glib-2.0 -I../usr/include/gtk-2.0 -I/usr/include/glib-2.0/glib -I/usr/lib/x86_64-linux-gnu/glib-2.0/include -I/usr/include/dbus-1.0/dbus -I/usr/include/dbus-1.0 -I/usr/lib/x86_64-linux-gnu/dbus-1.0/include -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


