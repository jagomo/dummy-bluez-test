/*
 ============================================================================
 Name        : bletest.c
 Author      : 
 Version     :
 Copyright   : Your copyright notice
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#define _GNU_SOURCE

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include "btgatt-server-ros.h"

#define DEFAULT_ADAPTER "hci0"
#define DEFAULT_ROBOTNAME "dot0"

int main(void) {
	puts("!!!Hello World!!!"); /* prints !!!Hello World!!! */

	launch_server(DEFAULT_ROBOTNAME, DEFAULT_ADAPTER);

	return EXIT_SUCCESS;
}
