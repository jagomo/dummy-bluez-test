#ifndef __BTGATT_SERVER_ROS_H_
#define __BTGATT_SERVER_ROS_H_

//typedef void (*sc_vel_write_callback) (uint32_t id, uint16_t cmd, const uint8_t *payload, uint8_t len);

int launch_server(const char* server_name, const char* device);
//int stop_server();
//void feed_sc_pos(uint32_t id, uint16_t cmd, const uint8_t *payload, uint8_t len);
//void set_sc_vel_write_cb (sc_vel_write_callback callback);

#endif //__BTGATT_SERVER_ROS_H_
